import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouteReuseStrategy } from '@angular/router';

import { IonicModule, IonicRouteStrategy } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';


import { NgxsModule } from '@ngxs/store';
import { NgxsReduxDevtoolsPluginModule } from '@ngxs/devtools-plugin';
import { NgxsLoggerPluginModule } from '@ngxs/logger-plugin';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';

import { InputMsjPageModalModule } from '../app/modals/input-msj/input-msj-modal.module';

import { ServicesModule } from '@services';
import { MessagesState } from '@store/messages/messages.state';
import { ChatState } from '@store/chat/chat.state';

import { environment } from 'src/environments/environment.prod';

import { WebsocketService } from 'src/app/shared/websocket/websocket.service';

@NgModule({
  declarations: [AppComponent],
  entryComponents: [],
  imports: [
    BrowserModule,
    IonicModule.forRoot(), 
    AppRoutingModule,
    ServicesModule,
    NgxsModule.forRoot([ 
      MessagesState,
      ChatState,
    ], {developmentMode:!environment.production}),
    NgxsReduxDevtoolsPluginModule.forRoot(),
    NgxsLoggerPluginModule.forRoot(),
    InputMsjPageModalModule
   
    
  ],
  providers: [
    StatusBar,
    SplashScreen,
    WebsocketService,
    { provide: RouteReuseStrategy, useClass: IonicRouteStrategy }
  ],
  bootstrap: [AppComponent]
})
export class AppModule {}
