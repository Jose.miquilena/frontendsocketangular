import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class MessagesService {

  API = 'http://localhost:3000';

  constructor(private httpClient: HttpClient) {
    
   }

   async getMessages(){
     return await this.httpClient.get<any>(`${this.API}/messages`).toPromise();
   }

   
   async getMessageId(id:number){
     return await this.httpClient.get<any>(`${this.API}/message/${id}`).toPromise();
   }
}
