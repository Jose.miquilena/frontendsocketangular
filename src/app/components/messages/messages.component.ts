import { Component, OnInit } from '@angular/core';
import { Store, Select } from '@ngxs/store';
import { Observable } from 'rxjs';
import { AppStateModel } from '@store/app-state.interface';
import { GetMessages, GetMessagesId } from '@store/messages/messages.actions';

@Component({
  selector: 'messages',
  templateUrl: './messages.component.html',
  styleUrls: ['./messages.component.scss'],
})
export class MessagesComponent{


  @Select((state:AppStateModel)=> state.messages.messages)
  messages$: Observable<any>;

  @Select((state:AppStateModel)=> state.messages.mesaggeId)
  messageId$: Observable<any>;

  constructor(private store:Store) { }

  async getMessages(){
    await this.store.dispatch(new GetMessages());
  }

  async getMessageId(id:number){
    await this.store.dispatch(new GetMessagesId(id));
  }


}
