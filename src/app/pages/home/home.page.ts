import { Component, OnInit } from '@angular/core';
import { Store, Select } from '@ngxs/store';
import { AppStateModel } from '@store/app-state.interface';
import { Observable } from 'rxjs';
import { ReceiveChat, GetUsers, SendChat } from '@store/chat/chat.actions';
import { ChatService } from '@services';
import { WebsocketService } from 'src/app/shared/websocket/websocket.service';
import { Kiosk } from 'src/app/models/kiosk';
import { NavController } from '@ionic/angular';


@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage implements OnInit {
  


  public Kioskoname: string = '';
  public email: string = '';

  
  public message: string = '';
  public messages: string[] = [];
  public users: number = 0;


  constructor(private store: Store, private chatService: ChatService, private wsService: WebsocketService, private navCtrl: NavController) {}

  ngOnInit(){
    

  }


  ingresar(){

    if (this.Kioskoname!==''){
      this.wsService.connect(this.Kioskoname);

      localStorage.setItem("kiosko", this.Kioskoname);

      this.navCtrl.navigateForward('kiosk-mensajes');
    }
  

  }

  
  
}
