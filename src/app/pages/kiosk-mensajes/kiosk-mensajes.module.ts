import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { KioskMensajesPage } from './kiosk-mensajes.page';

const routes: Routes = [
  {
    path: '',
    component: KioskMensajesPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [KioskMensajesPage]
})
export class KioskMensajesPageModule {}
