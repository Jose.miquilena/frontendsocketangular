import { Component, OnInit } from '@angular/core';
import { ChatService } from '@services';
import { WebsocketService } from 'src/app/shared/websocket/websocket.service';
import { NavController, ModalController } from '@ionic/angular';
import { InputMsjPageModal } from '../../modals/input-msj/input-msj-modal';

@Component({
  selector: 'app-kiosk-mensajes',
  templateUrl: './kiosk-mensajes.page.html',
  styleUrls: ['./kiosk-mensajes.page.scss'],
})
export class KioskMensajesPage implements OnInit {

  public messages: string[] =[];
  public message: string = '';
  public cantKiosk: number = 0;
  public kioskos: string[] = [];
  public msjPersonal: string[] = [];
  public enableInput: boolean = false;
  public kiosko : string = localStorage.getItem('kiosko');

  private modalMsj: HTMLIonModalElement;

  constructor(private chatService: ChatService, private wsService: WebsocketService, private navCtrl: NavController, private modalCtrl: ModalController,) { }

  ngOnInit() {
      
    this.chatService.onMessage().subscribe((message:string)=>{
        this.messages.push(message);
    });

    this.chatService.getKioskos().subscribe((kioskos: string[])=>{
      
      this.kioskos = kioskos;
    });


    this.chatService.onMymsj().subscribe((msj: string)=>{

      let pos = msj.indexOf(':');
      let kiosko = msj.substring(0,pos);
      if (kiosko == this.kiosko){
          let mensaje = msj.substring(pos+1);
          this.msjPersonal.push(mensaje);
      }
       
    });


    this.chatService.onNotificationPromo().subscribe((promo:any)=>{})
    

  }


  sendMsj(msj, kio){

    let date = new Date();
    let dateString = date.toString();
    
    let data = {
      message: msj,
      kiosko: kio,
      date: dateString
    }

   this.wsService.emit('messageKiosko',data);
  }

  async enabledMsj(kio){

    let title = {
      kiosko : kio
    }
    this.modalMsj = await this.modalCtrl.create({
      componentProps: {
        'title': kio
      },
      component: InputMsjPageModal
    });

    this.modalMsj.onDidDismiss().then((data) => {
        if (data != null && data.data != undefined && typeof data.data === 'string') {
          this.sendMsj(data.data, kio);
      }
    });

    this.modalMsj.present();

  }

 disconnet(){
    this.wsService.disconnect();
    this.navCtrl.navigateForward('home');

  }

  addChat(){
    this.messages.push(this.message);
    this.chatService.sendMessage(this.message);
    this.message = '';
  }


}
