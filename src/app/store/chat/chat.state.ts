import { ChatStateModel } from "./chat.state.model";
import { Store, State, Action, StateContext } from '@ngxs/store';
import { ChatService } from '@services';
import { SendChat, ReceiveChat, GetUsers } from './chat.actions';


@State<ChatStateModel>({
    name: 'chat',
    defaults:{
        users: 0,
        messages: null,
        message: null
    }
})
export class ChatState {
    constructor(private store: Store, private chatService: ChatService){}
    
    // @Action(SendChat)
    // sendChat(ctx: StateContext<ChatStateModel>, action: SendChat){

    //     const { message } = action;


    //     this.chatService.sendChat(message);

    //     ctx.patchState({
    //         message
    //     });
        
    // }

    // @Action(ReceiveChat)
    // receiveChat(ctx: StateContext<ChatStateModel>, action: ReceiveChat){
        
    //     let messages : string[] = [];

    //     // this.chatService.receiveChat().subscribe((message:string)=>{
             
    //         messages.push(message);
    //      });
        
        
    //      ctx.patchState({
    //         messages
    //     });
    // }

    // @Action(GetUsers)
    // getUsers(ctx: StateContext<ChatStateModel>, action: GetUsers){
    //    let users : number = 0;
       
    //     this.chatService.getUsers().subscribe((cantUser: number)=>{
    //         users = cantUser
    //     });
    
    //     ctx.patchState({
    //         users
    //     })
    // }

}