import { MessagesStateModel } from './messages/messages.state.model';
import { ChatStateModel } from './chat/chat.state.model';
export interface AppStateModel{
    messages: MessagesStateModel,
    chat: ChatStateModel
}