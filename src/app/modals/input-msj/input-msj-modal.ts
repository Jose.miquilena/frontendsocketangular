import { Component, AfterViewInit, ViewChild, ElementRef, Input } from '@angular/core';

import { ModalController, NavParams } from '@ionic/angular';

@Component({
    selector: 'input-msj-modal',
    templateUrl: 'input-msj-modal.html',
    styleUrls:['input-msj-modal.scss']
})

export class InputMsjPageModal {

    @Input() title: string;
    msj: string = "";
    
    

    constructor(public modalController: ModalController, navParams: NavParams) {
        
     }

   

    ok() {
              this.modalController.dismiss(this.msj);
    }

    cancel() {
        this.modalController.dismiss(null);
    }

   
}